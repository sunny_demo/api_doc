# 接入文档

  - [概述](#概述)
    - [商户接入步骤](#商户接入步骤)
    - [商户后台登录地址](#商户后台登录地址)
    - [平台异步通知来源IP](#平台异步通知来源ip)
    - [API域名(domain)](#api域名domain)
    - [异步通知策略](#异步通知策略)
    - [签名算法](#签名算法)
  - [API](#api)
    - [充值请求API](#充值请求api)
    - [充值异步通知API](#充值异步通知api)
    - [提现请求API](#提现请求api)
    - [提现异步通知API](#提现异步通知api)
    - [订单查询API](#订单查询api)
  - [附录](#附录)
    - [订单状态列表](#订单状态列表)
    - [支付类型列表](#支付类型列表)
    - [支付方式列表](#支付方式列表)
    - [支付方式与支付类型map表](#支付方式与支付类型map表)
    - [银行类型列表](#银行类型列表)
    - [错误码列表](#错误码列表)


# 概述


#### 商户接入步骤
- 请商户提供常用登录商户后台的客户端IP地址，只有这些IP才被允许登录商户后台
- 请向平台相关技术人员获取登录商户后台的用户名和初始密码，登录成功后请及时修改初始密码
- 请在商户后台获取商户ID和签名密钥，所有API请求使用密钥签名
- 请在商户后台配置API请求的来源IP白名单，只有来自这些IP的API请求才被允许
- 请将平台IP加入到异步通知请求中进行验证，只有来自这些IP的异步通知才被允许
- 请务必进行异步通知来源IP验证，如因没有进行异步通知IP来源验证被外部攻击造成的损失，需商户自行承担


#### 商户后台登录地址
    
    https://mch.sxhcwb.cn


#### 平台异步通知来源IP

    18.162.236.5
    18.162.143.27
    18.162.123.175
    18.162.123.212
    18.163.6.156
    18.162.156.134


#### API域名(domain)

    gateway.sxhcwb.com


#### 异步通知策略
- 订单状态为`成功`或者`失败`都会进行异步通知，异步通知可能重复推送，请自行处理重复通知可能导致的重复上分问题
- 定时通知时间间隔：30秒、60秒、1分钟、2分钟、5分钟、10分钟、30分钟
- 当异步通知次数用完后，商户可自行在商户后台的订单列表中找到对应的订单进行`补发通知`


#### 签名算法
- 先把参数字典按key的字母序列从小到大排序
- 用"="组合key和value，再用"&"组合所有的kv键值对，再追加secret_key的kv键值对
- 最后将组合的字符串进行utf8编码，转换为小写md5值
- 所有参与签名的字段值，字符串不能为空，数值不能为0
- python3示例代码：

        >>> import hashlib
        >>> secret_key = "jBeyka%JbTuP_t#A1rx3SqpL*7XiIfNQ-oms&C925OUG)K+h(=w8^cFldEZ4RMW!"
        >>> params = {"zzzz":341432141243,"xxxx":"234113412","yyyy":"中文","ffff":34143.2141243}
        >>> keys = sorted(list(params.keys()))
        >>> raw_str = '&'.join(["=".join(map(str, [k, params[k]])) for k in keys])
        >>> raw_str += '&secret_key=' + secret_key
        >>> print(raw_str)
        ffff=34143.2141243&xxxx=234113412&yyyy=中文&zzzz=341432141243&secret_key=jBeyka%JbTuP_t#A1rx3SqpL*7XiIfNQ-oms&C925OUG)K+h(=w8^cFldEZ4RMW!
        >>> utf8_bytes = raw_str.encode('utf8')
        >>> print(utf8_bytes)
        b'ffff=34143.2141243&xxxx=234113412&yyyy=\xe4\xb8\xad\xe6\x96\x87&zzzz=341432141243&secret_key=jBeyka%JbTuP_t#A1rx3SqpL*7XiIfNQ-oms&C925OUG)K+h(=w8^cFldEZ4RMW!'
        >>> sign = hashlib.md5(utf8_bytes).hexdigest()
        >>> print(sign)
        451eba640211f1552bb5f58a8caa56dc


# API

### 充值请求API

用户发起充值请求

> URL: https://{domain}/api/gateway/v1/deposit/request
>
> Method: POST
>
> Content-Type: application/json; charset=utf-8

##### 请求参数：

| 字段名  | 类型 | 必填  | 参与签名  | 说明  |
|---|---|---|---|---|
| sign  | string  | 是  | `否`  | 签名  |
| merchant_id  | integer  | 是  | 是  | 商户ID  |
| amount  | string  | 是  | 是  | 充值金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| mch_tx_id  | string  | 是  | 是  | 商户订单号，最大128个字符  |
| payment_type  | string  | 是  | 是  | 支付类型，参考文档尾部的“支付类型列表”  |
| payment_method  | string  | 是  | `否`  | 支付方式，参考文档尾部的“支付方式列表” |
| notify_url  | string  | 是  | 是  | 异步通知URL  |
| user_id  | string  | 是  | `否`  | 用户ID，最大32个字符，不参与签名  |
| user_ip  | string  | 是  | 是  | 用户客户端IP  |
| result_url  | string  | `否`  | `否`  | 充值结果展示的重定向URL，根据通道的实际情况决定是否跳转  |
| extra  | string  | `否`  | `否`  | 透传数据，在异步通知里面传回  |


例子数据:

    {
      "sign": "string",
      "merchant_id": 200,
      "amount": "500.34",
      "mch_tx_id": "string",
      "payment_type": "ZHIFUBAO",
      "notify_url": "https://google.com",
      "user_id": "999",
      "user_ip": "192.168.1.1",
      "result_url": "https://google.com",
      "extra": "string"
    }

##### 响应参数：

| 字段名  | 类型 | 必填 | 说明  |
|---|---|---|---|
| redirect_url  | string | 是  | 支付跳转URL  |
| valid_time  | integer | 是  | 订单有效时间，单位秒 |
| sys_tx_id  | string | 是  | 平台订单号，最长32位字符串 |
| mch_tx_id  | string | 是  | 商户订单号，最大128个字符 |

例子数据：

    {
      "error_code": 200,
      "message": "请求成功",
      "data": {
        "redirect_url": "https://google.com",
        "valid_time": 1800,
        "sys_tx_id": "string",
        "mch_tx_id": "string"
      }
    }


### 充值异步通知API

该接口需商户实现给平台调用

> URL: 充值请求中的notify_url
>
> Method: POST
>
> Content-Type: application/json; charset=utf-8


##### 请求参数：

| 字段名  | 类型 | 必填  | 参与签名  | 说明  |
|---|---|---|---|---|
| sign  | string  | 是  | `否`  | 签名  |
| merchant_id  | integer  | 是  | 是  | 商户ID  |
| state  | string  | 是  | 是  | 订单状态，SUCCESS: 成功, FAIL: 失败, 异步通知只会返回这两个状态之一  |
| amount  | string  | 是  | 是  | 充值金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| tx_amount  | string  | 是  | 是  | 实际交易金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| mch_tx_id  | string  | 是  | 是  | 商户订单号，最大128个字符  |
| sys_tx_id  | string  | 是  | 是  | 平台订单号，最大32个字符  |
| extra  | string  | `否`  | `否`  | 透传数据  |

例子数据:

    {
      "sign": "string",
      "merchant_id": 200,
      "state": "SUCCESS",
      "amount": "500.34",
      "tx_amount": "500.34",
      "mch_tx_id": "string",
      "sys_tx_id": "string",
      "extra": "string"
    }


##### 响应格式json：

如果商户认为`不需要`平台继续通知，返回`error_code等于200`，平台将停止发送通知，跟通知状态state是SUCCESS还是FAIL没有关系；  
如果商户认为`还需要`平台继续通知，返回`error_code不等于200`，平台将按异步通知频率进行重试通知，直到重试次数用完；  
当HTTP状态码非200时，平台将会重试通知；  
当响应内容不是下列`json`格式时，平台将`不再`重试通知，待商户修复错误后，商户自行在商户后台手动`补发通知`；  

    {
      "error_code": 200,    # 必填
      "message": "请求成功", # 可选填
    }



### 提现请求API

用户发起提现请求

> URL: https://{domain}/api/gateway/v1/withdraw/request
>
> Method: POST
>
> Content-Type: application/json; charset=utf-8

##### 请求参数：

| 字段名  | 类型 | 必填  | 参与签名  | 说明  |
|---|---|---|---|---|
| sign  | string  | 是  | `否`  | 签名  |
| merchant_id  | integer  | 是  | 是  | 商户ID  |
| amount  | string  | 是  | 是  | 充值金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| user_fee  | string  | 是  | `否`  | 扣用户的手续费，单位“元”，必须是整数或浮点数字符串，最多保留2位小数。如果没有扣手续费的需求，传0即可。user_fee是商户侧已经扣除用户的提现手续费。比如用户想提现100，商户侧想收取用户提现手续费2元，那么实际发起的提现金额amount就是98；如果支付侧的提现通道最低限额是100，那么发起金额amount为98就低于最低提现金额了，会导致提现发起失败；所以支付侧需要把实际发起金额amount为98加上商户那边扣掉了手续费user_fee为2元，才能满足最低限额，才能正常发起提现；user_fee在支付侧只会做这个条件判断，没有任何其它用处，也不会存储记录。|
| mch_tx_id  | string  | 是  | 是  | 商户订单号，最大128个字符  |
| notify_url  | string  | 是  | 是  | 异步通知URL  |
| bank_type  | string  | 是  | 是  | 银行类型，参考文档尾部“银行类型列表”  |
| card_no  | string  | 是  | 是  | 收款卡号  |
| account_name  | string  | 是  | 是  | 收款人姓名  |
| province  | string  | 是  | 是  | 省份名称  |
| city  | string  | 是  | 是  | 城市名称  |
| branch  | string  | 是  | `否`  | 支行名称  |
| user_id  | string  | 是  | `否`  | 用户ID，最大32个字符，不参与签名  |
| user_ip  | string  | 是  | 是  | 用户客户端IP  |
| extra  | string  | `否`  | `否`  | 透传数据，在异步通知里面传回  |


例子数据:

    {
      "sign": "string",
      "merchant_id": 200,
      "amount": "500.34",
      "mch_tx_id": "string",
      "notify_url": "https://google.com",
      "user_id": "999",
      "user_ip": "192.168.1.1",
      "bank_type": "ZHONGGUO",
      "card_no": "93838294912",
      "account_name": "张三",
      "branch": "中国银行深圳支行",
      "province": "广东省",
      "city": "深圳市",
      "extra": "string"
    }


##### 响应参数：

| 字段名  | 类型 | 必填 | 说明  |
|---|---|---|---|
| sys_tx_id  | string | 是  | 平台订单号，最长32位字符串 |
| mch_tx_id  | string | 是  | 商户订单号，最大128个字符 |

例子数据：

    {
      "error_code": 200,
      "message": "请求成功",
      "data": {
        "sys_tx_id": "string",
        "mch_tx_id": "string"
      }
    }



### 提现异步通知API

该接口需商户实现给平台调用

> URL: 提现请求中的notify_url
>
> Method: POST
>
> Content-Type: application/json; charset=utf-8


##### 请求参数：

| 字段名  | 类型 | 必填  | 参与签名  | 说明  |
|---|---|---|---|---|
| sign  | string  | 是  | `否`  | 签名  |
| merchant_id  | integer  | 是  | 是  | 商户ID  |
| state  | string  | 是  | 是  | 订单状态，SUCCESS: 成功, FAIL: 失败, 异步通知只会返回这两个状态之一  |
| amount  | string  | 是  | 是  | 充值金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| tx_amount  | string  | 是  | 是  | 实际交易金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| mch_tx_id  | string  | 是  | 是  | 商户订单号，最大128个字符  |
| sys_tx_id  | string  | 是  | 是  | 平台订单号，最大32个字符  |
| extra  | string  | `否`  | `否`  | 透传数据  |

例子数据:

    {
      "sign": "string",
      "merchant_id": 200,
      "state": "SUCCESS",
      "amount": "500.34",
      "tx_amount": "500.34",
      "mch_tx_id": "string",
      "sys_tx_id": "string",
      "extra": "string"
    }


##### 响应格式json：

如果商户认为`不需要`平台继续通知，返回`error_code等于200`，平台将停止发送通知，跟通知状态state是SUCCESS还是FAIL没有关系；  
如果商户认为`还需要`平台继续通知，返回`error_code不等于200`，平台将按异步通知频率进行重试通知，直到重试次数用完；  
当HTTP状态码非200时，平台将会重试通知；  
当响应内容不是下列`json`格式时，平台将`不再`重试通知，待商户修复错误后，商户自行在商户后台手动`补发通知`；  

    {
      "error_code": 200,    # 必填
      "message": "请求成功", # 可选填
    }


### 订单查询API

商户发起订单查询，当商户不在异步通知中进行上分等操作时，可以通过该接口查询订单状态进行上分操作

> URL: https://{domain}/api/gateway/v1/order/query
>
> Method: POST
>
> Content-Type: application/json; charset=utf-8

##### 请求参数：

| 字段名  | 类型 | 必填  | 参与签名  | 说明  |
|---|---|---|---|---|
| sign  | string  | 是  | `否`  | 签名  |
| merchant_id  | integer  | 是  | 是  | 商户ID  |
| sys_tx_id  | string  | 是  | 是  | 平台订单号，最大32个字符  |
| timestamp  | string  | 是  | 是  | 当前时间戳(如：1575365927)  |


例子数据:

    {
      "sign": "string",
      "merchant_id": 200,
      "sys_tx_id": "string",
      "timestamp": "1575365927"
    }


##### 响应参数：

| 字段名  | 类型 | 必填 | 说明  |
|---|---|---|---|
| merchant_id  | integer  | 是  | 商户ID  |
| state  | string  | 是  | 状态，参考"订单状态列表", 订单查询会返回订单的详细状态  |
| amount  | string  | 是  | 充值金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| tx_amount  | string  | 是  | 实际交易金额，单位“元”，必须是整数或浮点数字符串，最多保留2位小数  |
| mch_tx_id  | string  | 是  | 商户订单号，最大128个字符  |
| sys_tx_id  | string  | 是  | 平台订单号，最大32个字符  |

例子数据：

    {
      "error_code": 200,
      "message": "请求成功",
      "data": {
        "merchant_id": 200,
        "amount": "500.34",
        "tx_amount": "500.34",
        "mch_tx_id": "string",
        "sys_tx_id": "string",
        "state": "SUCCESS"
      }
    }


# 附录

### 订单状态列表

处理中的状态包括：INIT、ALLOC、DEALING  
处理成功的状态包括：SUCCESS  
处理失败的状态包括：FAIL、REFUND  
提现订单处理失败之后，会把提前扣掉的商户余额进行退款  

    {
        INIT: "订单生成",
        ALLOC: "已认领",
        DEALING: "处理中",
        SUCCESS: "成功",
        FAIL: "失败",
        REFUND: "已退款",
    }


### 支付类型列表

    {
      支付宝: ZHIFUBAO,
      微信: WEIXIN,
      银联: YINLIAN,
      云闪付: YUNSHANFU,
      银行卡: BANKCARD
    }


### 支付方式列表

    {
        支付宝H5: ZHIFUBAO_H5
        支付宝扫码: ZHIFUBAO_SAOMA
        微信H5: WEIXIN_H5
        微信扫码: WEIXIN_SAOMA
        银行卡转银行卡: BANK_TO_BANK
        支付宝转银行卡: ZHIFUBAO_TO_BANK
        微信转银行卡: WEIXIN_TO_BANK
        银联快捷: YINLIAN_KUAIJIE
        银联扫码: YINLIAN_SAOMA
        云闪付: YUNSHANFU
    }


### 支付方式与支付类型map表

    {
        ZHIFUBAO_H5: ZHIFUBAO,
        ZHIFUBAO_SAOMA: ZHIFUBAO,
        ZHIFUBAO_TO_BANK: ZHIFUBAO,
        WEIXIN_H5: WEIXIN,
        WEIXIN_SAOMA: WEIXIN,
        WEIXIN_TO_BANK: WEIXIN,
        BANK_TO_BANK: BANKCARD,
        YINLIAN_KUAIJIE: YINLIAN,
        YINLIAN_SAOMA: YINLIAN,
        YUNSHANFU: YUNSHANFU,
    }


### 银行类型列表

    {
      中国银行: ZHONGGUO,
      中国工商银行: GONGSHANG,
      中国建设银行: JIANSHE,
      中国农业银行: NONGYE,
      中国邮政储蓄银行: YOUZHENG,
      招商银行: ZHAOSHANG,
      浦东发展银行: PUFA,
      中国民生银行: MINSHENG,
      深发/平安银行: PINGAN,
      华夏银行: HUAXIA,
      中信银行: ZHONGXIN,
      中国光大银行: GUANGDA,
      兴业银行: XINGYE,
      广发银行: GUANGFA,
      中国交通银行: JIAOTONG
    }


### 错误码列表

    {
      "error_code": 1000,
      "message": "参数错误"
    }

    {
      "error_code": 10000,
      "message": "IP不在白名单内"
    }

    {
      "error_code": 10001,
      "message": "签名校验失败"
    }

    {
      "error_code": 10002,
      "message": "无可用通道"
    }

    {
      "error_code": 10003,
      "message": "充值发起失败"
    }
    
    {
      "error_code": 10004,
      "message": "提现发起失败"
    }

    {
      "error_code": 10005,
      "message": "订单不存在"
    }


